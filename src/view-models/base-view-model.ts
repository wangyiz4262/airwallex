import { ILoggingService, LoggingService } from "../services/logging-service";

export abstract class BaseViewModel {
  protected readonly logger: ILoggingService;

  protected constructor() {
    this.logger = LoggingService.getInstance(this.namespace);
  }

  protected get namespace(): string {
    return BaseViewModel.name;
  }
}
