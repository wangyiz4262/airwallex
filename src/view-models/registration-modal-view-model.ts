import { of } from "rxjs";
import { catchError, finalize, map, tap, timeout } from "rxjs/operators";
import { action, computed, makeObservable, observable } from "mobx";
import {
  IErrorResponse,
  IRegistrationRequestBody,
  RegistrationDataService,
} from "../services/registration-data-service";
import { BaseViewModel } from "./base-view-model";
import { LogContextEnum, LogLevelEnum } from "../services/logging-service";

export interface IRegistrationViewModalParams {
  onCompleteRegistration(): void;
}

export interface IRegistrationViewModel {
  readonly errorMessage: string;
  readonly fullName: string;
  readonly email: string;
  readonly confirmEmail: string;
  readonly fullNameErrorMessage: string;
  readonly emailErrorMessage: string;
  readonly confirmEmailErrorMessage: string;
  readonly isSubmitButtonEnabled: boolean;
  readonly requestInFlight: boolean;
  setFullName(fullName: string): void;
  setEmail(email: string): void;
  setConfirmEmail(email: string): void;
  submitRequestForInvite(finalizeCallback?: Function): void;
}

export class RegistrationViewModel
  extends BaseViewModel
  implements IRegistrationViewModel
{
  private readonly SUBMISSION_TIMEOUT;
  private readonly onCompleteRegistration: () => void;
  public fullName: string;
  public email: string;
  public confirmEmail: string;
  public errorMessage: string;
  public requestInFlight: boolean;

  protected constructor(params: IRegistrationViewModalParams) {
    super();
    makeObservable(this, {
      errorMessage: observable,
      fullName: observable,
      email: observable,
      confirmEmail: observable,
      requestInFlight: observable,
      fullNameErrorMessage: computed,
      emailErrorMessage: computed,
      confirmEmailErrorMessage: computed,
      isSubmitButtonEnabled: computed,
      setErrorMessage: action,
      setFullName: action,
      setEmail: action,
      setConfirmEmail: action,
      submitRequestForInvite: action,
    });
    this.SUBMISSION_TIMEOUT = 60000;
    this.requestInFlight = false;
    this.fullName = "";
    this.email = "";
    this.confirmEmail = "";
    this.errorMessage = "";
    this.onCompleteRegistration = params.onCompleteRegistration;
  }

  protected get namespace() {
    return RegistrationViewModel.name;
  }

  public get fullNameErrorMessage() {
    return this.fullName.length < 3
      ? "Full name should contain at least 3 characters"
      : "";
  }

  public get emailErrorMessage() {
    const regex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(this.email) ? "" : "The email entered is not valid";
  }

  public get confirmEmailErrorMessage() {
    return this.email === this.confirmEmail ? "" : `Email doesn't match`;
  }

  public get isSubmitButtonEnabled() {
    return (
      !this.fullNameErrorMessage &&
      !this.emailErrorMessage &&
      !this.confirmEmailErrorMessage &&
      !this.requestInFlight
    );
  }

  public static getInstance(
    params: IRegistrationViewModalParams
  ): IRegistrationViewModel {
    return new RegistrationViewModel(params);
  }

  public submitRequestForInvite = (finalizeCallback?: Function) => {
    const that = this;
    const payload: IRegistrationRequestBody = {
      name: this.fullName,
      email: this.email,
    };
    this.requestInFlight = true;
    this.setErrorMessage("");
    this.logger.logWithContext(
      LogLevelEnum.Info,
      LogContextEnum.ServiceRequest,
      payload
    );
    RegistrationDataService.getInstance()
      .submitRequest(payload)
      .pipe(
        map(({ response }) => response),
        tap((response) => {
          this.logger.logWithContext(
            LogLevelEnum.Info,
            LogContextEnum.ServerResponse,
            response
          );
          this.onCompleteRegistration();
        }),
        catchError(({ response }) => {
          const { errorMessage } = response as IErrorResponse;
          this.logger.logWithContext(
            LogLevelEnum.Error,
            LogContextEnum.ServerResponse,
            errorMessage
          );
          this.setErrorMessage(errorMessage);
          return of(errorMessage);
        }),
        timeout(this.SUBMISSION_TIMEOUT),
        finalize(
          action(() => {
            this.requestInFlight = false;
            finalizeCallback && finalizeCallback();
          })
        )
      )
      .subscribe({
        error(err) {
          that.logger.logWithContext(
            LogLevelEnum.Error,
            LogContextEnum.ServerResponse,
            err
          );
          that.setErrorMessage(err.message);
        },
      });
  };

  public setErrorMessage = (message: string) => {
    this.errorMessage = message;
  };

  public setFullName = (fullName: string) => {
    this.fullName = fullName;
    this.setErrorMessage("");
  };

  public setEmail = (email: string) => {
    this.email = email;
    this.setErrorMessage("");
  };

  public setConfirmEmail = (confirmEmail: string) => {
    this.confirmEmail = confirmEmail;
    this.setErrorMessage("");
  };
}
