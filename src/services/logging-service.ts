export enum LogLevelEnum {
  Info,
  Warn,
  Error,
}

export enum LogContextEnum {
  UserInteraction = "UserInteraction",
  ServiceRequest = "ServiceRequest",
  ServerResponse = "ServerResponse",
}

export interface ILoggingService {
  logWithContext(
    level: LogLevelEnum,
    context: LogContextEnum,
    ...messages: any[]
  ): void;
}

export class LoggingService implements ILoggingService {
  private readonly namespace: string;

  private constructor(namespace: string) {
    this.namespace = namespace;
  }

  public static getInstance(namespace: string): ILoggingService {
    return new LoggingService(namespace);
  }

  public logWithContext(
    level: LogLevelEnum,
    context: LogContextEnum,
    ...messages: any[]
  ) {
    let logAction;
    if (level === LogLevelEnum.Info) {
      logAction = console.log;
    }
    if (level === LogLevelEnum.Warn) {
      logAction = console.warn;
    }
    if (level === LogLevelEnum.Error) {
      logAction = console.error;
    }
    logAction?.call(console, `[${this.namespace}] [${context}]`, ...messages);
  }
}
