import { Observable } from "rxjs";
import { ajax, AjaxResponse } from "rxjs/ajax";

export interface IRegistrationRequestBody {
  readonly name: string;
  readonly email: string;
}

export type IErrorResponse = {
  errorMessage: string;
};

export type IRegistrationResponse = string | IErrorResponse;

export interface IRegistrationDataService {
  submitRequest(
    payload: IRegistrationRequestBody
  ): Observable<AjaxResponse<IRegistrationResponse>>;
}

export class RegistrationDataService implements IRegistrationDataService {
  private static instance: IRegistrationDataService;

  public static getInstance() {
    if (!RegistrationDataService.instance) {
      return (RegistrationDataService.instance = new RegistrationDataService());
    }
    return RegistrationDataService.instance;
  }

  public submitRequest(payload: IRegistrationRequestBody) {
    return ajax<IRegistrationResponse>({
      url: "https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth",
      method: "POST",
      crossDomain: true,
      headers: { "Content-Type": "application/json" },
      body: payload,
    });
  }
}
