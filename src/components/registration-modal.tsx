import * as React from "react";
import { Button, Modal, Typography } from "antd";
import { observer } from "mobx-react";

import { Input } from "./input";
import {
  LogContextEnum,
  LoggingService,
  LogLevelEnum,
} from "../services/logging-service";
import {
  IRegistrationViewModalParams,
  IRegistrationViewModel,
  RegistrationViewModel,
} from "../view-models/registration-modal-view-model";

export interface IRegistrationModalProps extends IRegistrationViewModalParams {
  readonly visible: boolean;
  closeModal(): void;
}

@observer
export class RegistrationModal extends React.Component<
  IRegistrationModalProps,
  {}
> {
  private readonly viewModel: IRegistrationViewModel;

  private get confirmButton() {
    const { requestInFlight } = this.viewModel;
    const buttonText = requestInFlight ? "Sending. Please wait..." : "Send";
    return (
      <Button
        disabled={!this.viewModel.isSubmitButtonEnabled}
        onClick={this.handleSubmitButtonClick}
        className="submit-button"
      >
        {buttonText}
      </Button>
    );
  }

  constructor(props: IRegistrationModalProps) {
    super(props);
    this.viewModel = RegistrationViewModel.getInstance({
      onCompleteRegistration: props.onCompleteRegistration,
    });
  }

  render() {
    const {
      fullName,
      email,
      confirmEmail,
      setFullName,
      setEmail,
      setConfirmEmail,
      fullNameErrorMessage,
      emailErrorMessage,
      confirmEmailErrorMessage,
      errorMessage,
      requestInFlight,
    } = this.viewModel;
    return (
      <Modal
        closable={!requestInFlight}
        maskClosable={!requestInFlight}
        title="Request an invite"
        footer={this.confirmButton}
        visible={this.props.visible}
        onCancel={this.props.closeModal}
      >
        <Input
          allowClear
          placeholder="Full name"
          value={fullName}
          setValue={setFullName}
          errorMessage={fullNameErrorMessage}
        />
        <Input
          allowClear
          placeholder="Email"
          value={email}
          setValue={setEmail}
          errorMessage={emailErrorMessage}
        />
        <Input
          allowClear
          placeholder="Confirm email"
          value={confirmEmail}
          setValue={setConfirmEmail}
          errorMessage={confirmEmailErrorMessage}
        />
        {
          <Typography.Text type="danger" className="error-message">
            {errorMessage}
          </Typography.Text>
        }
      </Modal>
    );
  }

  private readonly handleSubmitButtonClick = () => {
    logger.logWithContext(
      LogLevelEnum.Info,
      LogContextEnum.UserInteraction,
      "User clicked on submit button"
    );
    this.viewModel.submitRequestForInvite();
  };
}

const logger = LoggingService.getInstance(RegistrationModal.name);
