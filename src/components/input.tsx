import * as React from "react";
import classNames from "classnames";
import { Input as RcInput, Tooltip, InputProps as IRcInputProps } from "antd";

import "./input.css";

export interface IInputProps extends IRcInputProps {
  readonly errorMessage?: string;
  setValue?(value: string): void;
}

export const Input = ({
  setValue,
  errorMessage,
  ...restProps
}: IInputProps) => {
  const [focus, setFocus] = React.useState(false);
  const handleChange = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      if (setValue) {
        setValue(value);
      }
    },
    [setValue]
  );
  const handleBlur = React.useCallback(() => {
    setFocus(false);
  }, []);
  const handleFocus = React.useCallback(() => {
    setFocus(true);
  }, []);
  return (
    <Tooltip
      visible={focus && !!errorMessage}
      title={errorMessage}
      placement="topLeft"
      className={classNames("input-container", {
        "has-error": errorMessage,
      })}
    >
      <RcInput
        {...restProps}
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
      />
    </Tooltip>
  );
};
