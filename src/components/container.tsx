import * as React from "react";
import { Button, Layout, Typography } from "antd";
import { CopyrightOutlined, HeartFilled } from "@ant-design/icons";
import { RegistrationModal } from "./registration-modal";
import { ConfirmationModal } from "./confirmation-modal";

import "./container.css";

const LayoutContent = () => {
  const [showRequestModal, setShowRequestModal] = React.useState(false);
  const [showConfirmationModal, setShowConfirmationModal] =
    React.useState(false);
  const openRegistrationRequestModal = React.useCallback(() => {
    setShowRequestModal(true);
  }, []);
  const closeRegistrationRequestModal = React.useCallback(() => {
    setShowRequestModal(false);
  }, [setShowRequestModal]);
  const closeConfirmationModal = React.useCallback(() => {
    setShowConfirmationModal(false);
  }, [setShowConfirmationModal]);
  const onCompleteRegistration = React.useCallback(() => {
    closeRegistrationRequestModal();
    setShowConfirmationModal(true);
  }, [closeRegistrationRequestModal, setShowConfirmationModal]);
  return (
    <Content className="layout-content">
      <div className="slogan-wrapper">
        <Typography.Text className="slogan">A better way</Typography.Text>
        <Typography.Text className="slogan">
          to enjoy every day.
        </Typography.Text>
      </div>
      <Typography.Text>Be the first to know when we launch.</Typography.Text>
      <Button onClick={openRegistrationRequestModal}>Request an invite</Button>
      <RegistrationModal
        visible={showRequestModal}
        closeModal={closeRegistrationRequestModal}
        onCompleteRegistration={onCompleteRegistration}
      />
      <ConfirmationModal
        onClose={closeConfirmationModal}
        visible={showConfirmationModal}
      />
    </Content>
  );
};

const { Header, Content, Footer } = Layout;

export const Container = () => {
  return (
    <Layout className="layout">
      <Header className="layout-header">BROCCOLI &amp; CO.</Header>
      <LayoutContent />
      <Footer className="layout-footer">
        <Typography.Text>
          Made with <HeartFilled style={{ fontSize: "0.8em" }} /> in Melborne.
        </Typography.Text>
        <Typography.Text>
          <CopyrightOutlined
            style={{ fontSize: "0.8em", marginRight: "2px" }}
          />
          2016 Broccoli &amp; Co. All rights reserved.
        </Typography.Text>
      </Footer>
    </Layout>
  );
};
