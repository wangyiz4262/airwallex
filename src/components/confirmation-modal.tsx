import { Button, Modal, Typography } from "antd";

export interface IConfirmModalProps {
  readonly visible: boolean;
  onClose(): void;
}

export const ConfirmationModal = (props: IConfirmModalProps) => {
  const content =
    "You will be one of the first to experience Broccoli & Co. when we launch";
  const confirmButton = <Button onClick={props.onClose}>OK</Button>;
  return (
    <Modal
      title="All done!"
      closable={false}
      footer={confirmButton}
      visible={props.visible}
    >
      <Typography.Text>{content}</Typography.Text>
    </Modal>
  );
};
