import "jest";
import * as React from "react";
import {
  fireEvent,
  getByTestId,
  getByText,
  render,
} from "@testing-library/react";
import { Container } from "../src/components/container";
import { IRegistrationModalProps } from "../src/components/registration-modal";
import { IConfirmModalProps } from "../src/components/confirmation-modal";

jest.mock("../src/components/registration-modal", () => ({
  RegistrationModal: ({
    visible,
    closeModal,
    onCompleteRegistration,
  }: IRegistrationModalProps) => (
    <div
      data-testid="registration-modal"
      className={visible.toString()}
      onClick={closeModal}
      onDoubleClick={onCompleteRegistration}
    />
  ),
}));

jest.mock("../src/components/confirmation-modal", () => ({
  ConfirmationModal: ({ visible, onClose }: IConfirmModalProps) => (
    <div
      data-testid="confirmation-modal"
      className={visible.toString()}
      onClick={onClose}
    />
  ),
}));

jest.mock("../src/components/container.css", () => ({}));

describe("Given component Container", () => {
  let container: HTMLElement;
  beforeEach(() => {
    container = render(<Container />).container;
  });

  describe("When component is mounted", () => {
    test("it should have necessary text and the button to open the modal", () => {
      expect(getByText(container, "BROCCOLI & CO.")).toBeDefined();
      expect(container.querySelectorAll(".slogan").length).toBe(2);
      expect(getByText(container, "Request an invite")).toBeDefined();
      expect(getByTestId(container, "registration-modal")).toBeDefined();
      expect(getByTestId(container, "confirmation-modal")).toBeDefined();
    });
  });

  describe("When clicking on the submission button", () => {
    test("registration-modal should visible prop as true", () => {
      expect(
        getByTestId(container, "registration-modal").classList.contains("false")
      );
      fireEvent.click(getByText(container, "Request an invite"));
      expect(
        getByTestId(container, "registration-modal").classList.contains("true")
      );
    });
  });

  describe("When registration-modal is open", () => {
    let registrationModal: HTMLElement;
    beforeEach(() => {
      fireEvent.click(getByText(container, "Request an invite"));
      registrationModal = getByTestId(container, "registration-modal");
    });

    test("click to close registration-modal should receive visible prop as false", () => {
      fireEvent.click(registrationModal);
      expect(registrationModal.classList.contains("false")).toBeTruthy();
    });

    test("submitting request successfully should close registration-modal and open confirmation-modal", () => {
      fireEvent.doubleClick(registrationModal);
      expect(registrationModal.classList.contains("false")).toBeTruthy();
      expect(
        getByTestId(container, "confirmation-modal").classList.contains("true")
      ).toBeTruthy();
    });
  });

  describe("When confirmation-modal is open", () => {
    test("click to close confirmation-modal should receive visible prop as false", () => {
      fireEvent.click(getByText(container, "Request an invite"));
      fireEvent.doubleClick(getByTestId(container, "registration-modal"));
      const confirmationModal = getByTestId(container, "confirmation-modal");
      fireEvent.click(confirmationModal);
      expect(confirmationModal.classList.contains("false")).toBeTruthy();
    });
  });
});
