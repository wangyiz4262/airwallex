import "jest";
import { throwError, timer } from "rxjs";
import {
  IRegistrationViewModel,
  RegistrationViewModel,
} from "../src/view-models/registration-modal-view-model";

const mockSubmitRequest: jest.Mock = jest.fn();
jest.mock("../src/services/registration-data-service", () => ({
  RegistrationDataService: class {
    static getInstance() {
      return { submitRequest: mockSubmitRequest };
    }
  },
}));

jest.mock("../src/services/logging-service", () => ({
  LoggingService: class {
    static getInstance() {
      return { logWithContext: jest.fn() };
    }
  },
  LogLevelEnum: { Info: 0, Warn: 1, Error: 2 },
  LogContextEnum: { UserInteraction: 0, ServiceRequest: 1, ServerResponse: 2 },
}));

describe("Given RegistrationViewModel", () => {
  let viewModel: IRegistrationViewModel;
  let mockOnCompleteRegistration = jest.fn();
  beforeEach(() => {
    viewModel = RegistrationViewModel.getInstance({
      onCompleteRegistration: mockOnCompleteRegistration,
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("When static method getInstance gets called", () => {
    test("viewModel should be an instance of RegistrationViewModel", () => {
      expect(viewModel).toBeInstanceOf(RegistrationViewModel);
    });

    test("each time calling getInstance will create different instances", () => {
      expect(viewModel).not.toBe(
        RegistrationViewModel.getInstance({
          onCompleteRegistration: mockOnCompleteRegistration,
        })
      );
    });
  });

  describe("When instantiated", () => {
    test("class members should be initialized", () => {
      expect(viewModel).toEqual(
        expect.objectContaining<Partial<IRegistrationViewModel>>({
          requestInFlight: false,
          fullName: "",
          email: "",
          confirmEmail: "",
          errorMessage: "",
          fullNameErrorMessage: expect.stringMatching(/^\w+/),
          emailErrorMessage: expect.stringMatching(/^\w+/),
          confirmEmailErrorMessage: "",
          isSubmitButtonEnabled: false,
        })
      );
    });
  });

  describe("When fullName is changed", () => {
    test("fullNameErrorMessage should present if fullName is not valid", () => {
      viewModel.setFullName("a");
      expect(viewModel.fullName).toBe("a");
      expect(viewModel.fullNameErrorMessage).toEqual(
        expect.stringMatching(/^\w+/)
      );
    });

    test("fullNameErrorMessage should be empty if fullName is valid", () => {
      viewModel.setFullName("abcd");
      expect(viewModel.fullName).toBe("abcd");
      expect(viewModel.fullNameErrorMessage).toBe("");
    });
  });

  describe("When email is changed", () => {
    test("emailErrorMessage should present if email is not valid", () => {
      viewModel.setEmail("abc@");
      expect(viewModel.email).toBe("abc@");
      expect(viewModel.emailErrorMessage).toEqual(
        expect.stringMatching(/^\w+/)
      );
    });

    test("emailErrorMessage should be empty if email is valid", () => {
      viewModel.setEmail("abc@email.com");
      expect(viewModel.email).toBe("abc@email.com");
      expect(viewModel.emailErrorMessage).toBe("");
    });
  });

  describe("When confirmEmail is changed", () => {
    test("confirmEmailErrorMessage should present if confirmEmail is not same as email", () => {
      viewModel.setEmail("abc@email.com");
      viewModel.setConfirmEmail("abc@email.co");
      expect(viewModel.confirmEmail).toBe("abc@email.co");
      expect(viewModel.confirmEmailErrorMessage).toEqual(
        expect.stringMatching(/^\w+/)
      );
    });

    test("confirmEmailErrorMessage should be empty if confirmEmail is same as email", () => {
      viewModel.setEmail("abc@email.com");
      viewModel.setConfirmEmail("abc@email.com");
      expect(viewModel.confirmEmail).toBe("abc@email.com");
      expect(viewModel.confirmEmailErrorMessage).toBe("");
    });
  });

  describe("When submitRequestForInvite gets called", () => {
    beforeEach(() => {
      jest.useFakeTimers();
    });

    test("valid response should trigger correct update", (done: jest.DoneCallback) => {
      mockSubmitRequest.mockReturnValue(timer(0));
      viewModel.submitRequestForInvite(() => {
        expect(viewModel.errorMessage).toBe("");
        expect(mockOnCompleteRegistration).toHaveBeenCalled();
        expect(viewModel.requestInFlight).toBeFalsy();
        done();
      });
      expect(mockOnCompleteRegistration).not.toHaveBeenCalled();
      expect(viewModel.requestInFlight).toBeTruthy();
      jest.runAllTimers();
    });

    test("response comes back too late and throws timeout error", (done) => {
      mockSubmitRequest.mockReturnValue(timer(61000));
      viewModel.submitRequestForInvite(() => {
        expect(viewModel.errorMessage).toEqual(
          expect.stringContaining("Timeout")
        );
        expect(mockOnCompleteRegistration).not.toHaveBeenCalled();
        expect(viewModel.requestInFlight).toBeFalsy();
        done();
      });
      expect(mockOnCompleteRegistration).not.toHaveBeenCalled();
      expect(viewModel.requestInFlight).toBeTruthy();
      jest.runAllTimers();
    });

    test("ajax returns error code", (done) => {
      mockSubmitRequest.mockReturnValue(
        throwError(() => ({ response: { errorMessage: "ajax error" } }))
      );
      viewModel.submitRequestForInvite(() => {
        expect(viewModel.errorMessage).toBe("ajax error");
        expect(mockOnCompleteRegistration).not.toHaveBeenCalled();
        done();
      });
      jest.runAllTimers();
    });
  });
});
