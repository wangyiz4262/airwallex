import "jest";
import { RegistrationDataService } from "../src/services/registration-data-service";

describe("Given RegistrationDataService", () => {
  describe("When the class is instantiated", () => {
    test("the instance should be a singleton", () => {
      expect(RegistrationDataService.getInstance()).toBe(
        RegistrationDataService.getInstance()
      );
    });
  });
});
