import "jest";
import * as React from "react";
import { timer } from "rxjs";
import { fireEvent, render, screen } from "@testing-library/react";
import { RegistrationModal } from "../src/components/registration-modal";
import { RegistrationDataService } from "../src/services/registration-data-service";

jest.mock("../src/components/input", () => {
  const React = require("react");
  const { Input: RcInput } = require("antd");
  return {
    Input(props: any) {
      const onChange = React.useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
          props.setValue(event.target.value);
        },
        [props.setValue]
      );
      return (
        <RcInput
          placeholder={props.placeholder}
          value={props.value}
          onChange={onChange}
        />
      );
    },
  };
});

jest.mock("../src/services/logging-service", () => ({
  LoggingService: class {
    static getInstance() {
      return { logWithContext: jest.fn() };
    }
  },
  LogLevelEnum: { Info: 0, Warn: 1, Error: 2 },
  LogContextEnum: { UserInteraction: 0, ServiceRequest: 1, ServerResponse: 2 },
}));

describe("Given component RegistrationModal", () => {
  let container: HTMLElement;
  const mockOnCompleteRegistration = jest.fn();
  beforeEach(() => {
    container = render(
      <RegistrationModal
        visible
        closeModal={jest.fn()}
        onCompleteRegistration={mockOnCompleteRegistration}
      />
    ).container;
  });

  describe("When component is mounted", () => {
    test("it should have three input boxes on the page", () => {
      expect(screen.getByPlaceholderText("Full name")).toBeDefined();
      expect(screen.getByPlaceholderText("Email")).toBeDefined();
      expect(screen.getByPlaceholderText("Confirm email")).toBeDefined();
    });

    test("it should how a submit button with text Send", () => {
      expect(
        screen
          .getAllByRole("button")
          .find((element) => element.classList.contains("submit-button"))
          ?.textContent
      ).toBe("Send");
    });

    test("it should how a submit button disabled", () => {
      expect(
        (
          screen
            .getAllByRole("button")
            .find((element) =>
              element.classList.contains("submit-button")
            ) as HTMLButtonElement
        ).disabled
      ).toBeTruthy();
    });

    test("it should not show errorMessage but with an empty tag", () => {
      expect(
        container.querySelector(".error-message")?.textContent
      ).toBeUndefined();
    });
  });

  describe("When value in fullName input box changes", () => {
    test("value should change accordingly", () => {
      const input: HTMLInputElement = screen.getByPlaceholderText("Full name");
      expect(input.value).toBe("");
      fireEvent.change(input, { target: { value: "a" } });
      expect(input.value).toBe("a");
    });
  });

  describe("When value in email input box changes", () => {
    test("value should change accordingly", () => {
      const input: HTMLInputElement = screen.getByPlaceholderText("Email");
      expect(input.value).toBe("");
      fireEvent.change(input, { target: { value: "a" } });
      expect(input.value).toBe("a");
    });
  });

  describe("When value in confirmEmail input box changes", () => {
    test("value should change accordingly", () => {
      const input: HTMLInputElement =
        screen.getByPlaceholderText("Confirm email");
      expect(input.value).toBe("");
      fireEvent.change(input, { target: { value: "a" } });
      expect(input.value).toBe("a");
    });
  });

  describe("When value changes in input box", () => {
    test("submit button should be disabled when full name is invalid", () => {
      const input: HTMLInputElement = screen.getByPlaceholderText("Full name");
      fireEvent.change(input, { target: { value: "a" } });
      expect(
        (
          screen
            .getAllByRole("button")
            .find((element) =>
              element.classList.contains("submit-button")
            ) as HTMLButtonElement
        ).disabled
      ).toBeTruthy();
    });

    test("submit button should be disabled when email is invalid", () => {
      const input: HTMLInputElement = screen.getByPlaceholderText("Email");
      fireEvent.change(input, { target: { value: "a" } });
      expect(
        (
          screen
            .getAllByRole("button")
            .find((element) =>
              element.classList.contains("submit-button")
            ) as HTMLButtonElement
        ).disabled
      ).toBeTruthy();
    });

    test("submit button should be disabled when confirm email is invalid", () => {
      const email: HTMLInputElement = screen.getByPlaceholderText("Email");
      const confirmEmail: HTMLInputElement =
        screen.getByPlaceholderText("Confirm email");
      fireEvent.change(email, { target: { value: "a@b.com" } });
      fireEvent.change(confirmEmail, { target: { value: "a@b.co" } });
      expect(
        (
          screen
            .getAllByRole("button")
            .find((element) =>
              element.classList.contains("submit-button")
            ) as HTMLButtonElement
        ).disabled
      ).toBeTruthy();
    });

    test("submit button should be enabled when all input fields are valid", () => {
      fireEvent.change(screen.getByPlaceholderText("Full name"), {
        target: { value: "abcd" },
      });
      fireEvent.change(screen.getByPlaceholderText("Email"), {
        target: { value: "a@b.com" },
      });
      fireEvent.change(screen.getByPlaceholderText("Confirm email"), {
        target: { value: "a@b.com" },
      });
      expect(
        (
          screen
            .getAllByRole("button")
            .find((element) =>
              element.classList.contains("submit-button")
            ) as HTMLButtonElement
        ).disabled
      ).toBeFalsy();
    });
  });

  describe("When click on submit button", () => {
    let button: HTMLButtonElement;
    beforeEach(() => {
      jest.useFakeTimers();
      jest
        .spyOn(RegistrationDataService, "getInstance")
        // @ts-ignore
        .mockImplementation(() => ({ submitRequest: () => timer(0) }));
      fireEvent.change(screen.getByPlaceholderText("Full name"), {
        target: { value: "abcd" },
      });
      fireEvent.change(screen.getByPlaceholderText("Email"), {
        target: { value: "a@b.com" },
      });
      fireEvent.change(screen.getByPlaceholderText("Confirm email"), {
        target: { value: "a@b.com" },
      });
      button = screen
        .getAllByRole("button")
        .find((element) =>
          element.classList.contains("submit-button")
        ) as HTMLButtonElement;
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    test("text on submit button changes", () => {
      fireEvent.click(button);
      expect(button.textContent).toEqual(expect.stringContaining("Sending"));
      jest.runAllTimers();
      expect(button.textContent).toEqual("Send");
    });

    test("submit button should be disabled when request being sent", () => {
      fireEvent.click(button);
      expect(button.disabled).toBeTruthy();
      jest.runAllTimers();
      expect(button.disabled).toBeFalsy();
    });

    test("onCompleteRegistration handler should get called", () => {
      expect(mockOnCompleteRegistration).not.toHaveBeenCalled();
      fireEvent.click(button);
      jest.runAllTimers();
      expect(mockOnCompleteRegistration).toHaveBeenCalledTimes(1);
    });
  });
});
